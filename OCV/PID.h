#pragma once
typedef struct
{
    /*Bounds*/
    double max_val;
    double min_val;
    /*Parameters*/
    double kp;
    double ki;
    double kd;

    double last_err;
    double iv;
    /*Process variables*/
    double *in;   //input
    double *ref;  //reference
    double samp_time;
} PID_State;

void pid_bounds(PID_State *ps, double min, double max);
void pid_adjust(PID_State *ps, double kp, double ki, double kd, double st);
double pid_update(PID_State *ps);
void pid_init(PID_State *ps, double kp, double ki, double kd, double samp_time, double *in, double *ref);