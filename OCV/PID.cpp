#include <stdio.h>
#include <PID.h>
#include <string.h>
/* PID controller - early implementation*/

#define min(x,y) ((x)<(y)?(x):(y))
#define max(x,y) ((x)>(y)?(x):(y))
#define clamp(x1,y,x2) ((min((x2),max((y),(x1)))))


void pid_init(PID_State *ps, double kp, double ki, double kd, double samp_time, double *in, double *ref)
{
    memset(ps, 0, sizeof(PID_State));
    ps->in = in;
    ps->ref = ref;
    pid_adjust(ps, kp, ki, kd, samp_time);
}

void pid_adjust(PID_State *ps, double kp, double ki, double kd, double st)
{
    ps->kp = kp;
    ps->ki = ki;
    ps->kd = kd;
    ps->samp_time = st;
}

void pid_bounds(PID_State *ps, double min, double max)
{
    ps->max_val = max;
    ps->min_val = min;
}

double pid_update(PID_State *ps)
{
    double out = 0.0;

    double error = (*ps->ref) - (*ps->in);                         //Error
    ps->iv += (error  * ps->samp_time);                   //Integral
    double dv = (error - ps->last_err) / ps->samp_time;  //Differential

    ps->iv = clamp(ps->min_val, ps->iv, ps->max_val);                          //make sure the integral part stays in bounds
    out = (error * ps->kp) + (ps->iv * ps->ki) + (dv * ps->kd);      //calculate the output (P+I+D)
    out = clamp(ps->min_val, out, ps->max_val);                       //keep the output in bounds
    ps->last_err = error;                                             //store the current error for the next cycle
#if 0
    printf("Ref %lf In %lf Out %lf\n", (*ps->ref), (*ps->in), out);
#endif
    return(out);
}


