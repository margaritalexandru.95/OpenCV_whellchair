#include "OCV.h"

#define CALIBRATION_TOLERANCE_PX 10
#define SAFE_TIMEOUT 2000

void init_ocv(OCV *o)
{
    o->vc = new VideoCapture(0);
    if (o->vc->isOpened() == 0)
    {
        delete o->vc;
        o->vc = NULL;
    }
    else
    {
        o->face = new CascadeClassifier(".\\haarcascades\\haarcascade_frontalface_alt2.xml");
        if (o->face == NULL || o->face->empty())
        {
            delete o->face;
            o->face = NULL;
        }
        else
        {
            o->eye = new CascadeClassifier(".\\haarcascades\\haarcascade_mcs_eyepair_big.xml");

            if (o->eye == NULL || o->eye->empty())
            {
                delete o->face;
                delete o->eye;
                o->eye = NULL;
                o->face = NULL;
            }
        }
    }
}

/*helper function to validate faces*/

inline int eye_in_face(Rect eye, Rect face)
{
    return(eye.x >= face.x &&
        eye.y >= face.y &&
        eye.x + eye.width <= face.x + face.width &&
        eye.y + eye.height <= face.y + face.height);
}

/* helper function used to transfer bitmaps from Mat to the CreateDIBSection() buffer*/
static void xfer_bitmap(unsigned char *pxs, unsigned char *pxd, int w, int h, unsigned char gsr)
{
    for (int th = 0; th < h; th++)
    {
        for (int tw = 0; tw < w; tw++)
        {
            if (gsr)
            {
                pxd[0] = pxs[0];
                pxd[1] = pxs[0];
                pxd[2] = pxs[0];
                pxs++;
            }
            else
            {
                pxd[0] = pxs[0];
                pxd[1] = pxs[1];
                pxd[2] = pxs[2];
                pxs += 3;
            }
            pxd += 3;
        }
        if (gsr == 0)
        {
            pxs += (w % 4);
        }
        pxd += (w % 4);
    }
}

static void identify_face(OCV *o, UMat &image, UMat &gray, UMat &egray)
{
    //setup boxes for face and eyes
    Rect t_face(0, 0, 0, 0);
    Rect t_eye(0, 0, 0, 0);
    vector< Rect > faces;   //face storage for detectMultiScale

    o->has_face = 0;                    //assume that no face is detected
    cvtColor(image, gray, CV_BGR2GRAY); //make the image grayscale as the detectMultiScale routine needs it.
	
   /* equalizeHist(gray, egray);*/          //increase the contrast of the grayed image
	egray = gray;

    //do the detection
    o->face->detectMultiScale(egray, faces, 1.1, 1, CV_HAAR_DO_ROUGH_SEARCH | CV_HAAR_FIND_BIGGEST_OBJECT | CV_HAAR_DO_CANNY_PRUNING, Size(100, 100), Size(1280, 1280));

    if (faces.size() >= 1) //if we have at least one face we will check if we have eyes
    {
        vector< Rect > eye_rect;    //eye storage for detectMultiScale

        o->eye->detectMultiScale(egray, eye_rect, 1.02, 1, CV_HAAR_DO_CANNY_PRUNING, Size(40, 20), Size(1280, 640));    //detect eyes

        rectangle(image.getMat(ACCESS_WRITE), faces[0], CV_RGB(0, 255, 0), 1);  //surround the face with a rectangle

        /* How it should work:
            * A valid face has both the face rectangle and the eye rectangle.
            * The eye rectangle must be within the bounds of the face rectangle.
            * If it's not, then the face is invalid and the next face from the vector faces is tested.
            * If a face is found then the loop is interrupted and the new coordinates for the center of the face
            *and for the center of the eyes are stored in the work_face and work eyes.
            */
		for (unsigned char i = 0; i < eye_rect.size(); i++)
		{

			/*
			*validate the pair of eyes with the current face
			*/
			if (eye_in_face(eye_rect[i], faces[0]))
			{
				o->no_face_timeout = 0;
				o->has_face = 1;        //set the face flag to 1 as we do have what we are waiting for

				rectangle(image.getMat(ACCESS_WRITE), eye_rect[i], CV_RGB(0, 255, 0), 1); //draw a nice rectangle for the eyes

				t_eye = eye_rect[i];                                //store the current eye in a temporary variable
				t_face = faces[0];                                  //store the current face in a temporary variable


				/*Timeout based calibration
				 1) Check if the face rectangle reamins almost the same (within a tolerance of 5 pixels)
				 2) If the tolerances are met, we start the calibration timer
				 3) After the timer expires, we store the current frame as as the calibration data
				 4) If by any chance the face rectangle goes out of the specified tolerance reset the timeout and go to 1
				 */
				if (o->md.run == 0)
				{
					long dx = labs(t_face.x - o->calib_face.x);
					long dy = labs(t_face.y - o->calib_face.y);
					long dw = labs(t_face.width - o->calib_face.width);
					long dh = labs(t_face.height - o->calib_face.height);

					if (o->calib_status < 2)
					{
						if (dx > CALIBRATION_TOLERANCE_PX ||
							dy > CALIBRATION_TOLERANCE_PX ||
							dw > CALIBRATION_TOLERANCE_PX ||
							dh > CALIBRATION_TOLERANCE_PX)
						{
							o->calib_timeout = GetTickCount64();
							o->calib_status = 1;
						}

						if (GetTickCount64() - o->calib_timeout >= 2000)
						{
							o->calib_status = 2;
						}
						o->calib_face = t_face;
					}
					
				}
				break;
			}
		}
    }

    if(o->has_face == 0)
    {   
		if (o->no_face_timeout == 0)
        {
			t_face = o->work_face;
            o->no_face_timeout = GetTickCount64();
        }
		else if (GetTickCount64() - o->no_face_timeout < SAFE_TIMEOUT && o->calib_status != 0)
		{
			t_face = o->work_face;
		}
        else if (GetTickCount64()-o->no_face_timeout >= 2000 && o->calib_status > 2)
        {
			o->work_face = Rect(0, 0, 0, 0);
			o->calib_status = 2;
			o->calib_timeout = 0;
        }
    }

   
	
    if (o->calib_status == 3)
    {		
        o->work_face = t_face;
    }
}


/*This  routine does not do anything related to the control of the wheelchair
* but it used to create the views in the application*/

void frame_provider(OCV *o, HWND win)
{

    UMat frame;             //working frame
    UMat window;            //data ready for window
    UMat window_markers;    //frame with markers
    UMat gray;      //grayed frame
    UMat egray;   //enhanced gray image
    void *bmp_start = NULL;

    if (o->vc)
    {
        o->vc->read(frame); //gather the frame from the camera
    }

    frame.copyTo(window);  //store the frame in a temporary variable for further processing

    if (o->face && o->eye)
    {
        identify_face(o, frame, gray, egray);                   //do the identification of the face (actually is detection)
    }

    frame.copyTo(window_markers);               //copy the frame processed in context of indentify_face()

    /*
    * Not very interesting -
    *  just initialize a device context with
    *  a bitmap to render the frames in their places
    */

    if (o->dc == NULL) //one time initialization
    {
        o->dc = CreateCompatibleDC(NULL);
    }

    if (o->bmph == NULL) //one time initialization
    {
        BITMAPINFO bi = { 0 };
        bi.bmiHeader.biBitCount = 24;
        o->bmp_height = bi.bmiHeader.biHeight = frame.rows;
        o->bmp_width = bi.bmiHeader.biWidth = frame.cols;
        bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        bi.bmiHeader.biCompression = BI_RGB;
        bi.bmiHeader.biPlanes = 1;
        o->bmph = CreateDIBSection(o->dc, &bi, DIB_RGB_COLORS, &o->px_data, NULL, 0);
        if (o->bmph)
        {
            o->dc_bmp = (HBITMAP)SelectObject(o->dc, o->bmph);
        }
    }

    HWND w = NULL;

    /*Do the drawing
    Not the best place to do the rendering but it gets the job done*/
    if (o->px_data)
    {
        if ((w = GetDlgItem(win, IDC_CAMERA_FRAME)) != NULL)
        {
            xfer_bitmap(window.getMat(ACCESS_READ).data, (unsigned char*)o->px_data, window.rows, window.cols, 0);
            RECT r = { 0 };
            GetClientRect(w, &r);
            HDC dc = GetDC(w);
            SetStretchBltMode(dc, HALFTONE);
            StretchBlt(dc, r.left, r.top, r.right, r.bottom, o->dc, o->bmp_width, o->bmp_height, -o->bmp_width, -o->bmp_height, SRCCOPY);
            ReleaseDC(w, dc);
        }

        if ((w = GetDlgItem(win, IDC_CAMERA_FRAME2)) != NULL)
        {
            xfer_bitmap(gray.getMat(ACCESS_READ).data, (unsigned char*)o->px_data, gray.rows, gray.cols, 1);
            RECT r = { 0 };
            GetClientRect(w, &r);
            HDC dc = GetDC(w);
            SetStretchBltMode(dc, HALFTONE);
            StretchBlt(dc, r.left, r.top, r.right, r.bottom, o->dc, o->bmp_width, o->bmp_height, -o->bmp_width, -o->bmp_height, SRCCOPY);
            ReleaseDC(w, dc);
        }
        if ((w = GetDlgItem(win, IDC_CAMERA_FRAME3)) != NULL)
        {
            xfer_bitmap(window_markers.getMat(ACCESS_READ).data, (unsigned char*)o->px_data, window_markers.rows, window_markers.cols, 0);
            RECT r = { 0 };
            GetClientRect(w, &r);
            HDC dc = GetDC(w);
            SetStretchBltMode(dc, HALFTONE);
            StretchBlt(dc, r.left, r.top, r.right, r.bottom, o->dc, o->bmp_width, o->bmp_height, -o->bmp_width, -o->bmp_height, SRCCOPY);
            ReleaseDC(w, dc);
        }
    }


    /*
    *   Show the coordinates obtained during calibration -
    * those coordinates are used as reference for the command handling
    */
    if ((w = GetDlgItem(win, IDC_CALIB_SHOW)) != NULL)
    {
        if (o->calib_status == 2)
        {
            o->calib_status = 3;
            wchar_t b[512] = { 0 };
            _snwprintf(b, 512, L"Face rectangle:\n\tX: %d\n\tY: %d\n\tW: %d\n\tH: %d\nFace center\t(%d, %d)\n",
                o->calib_face.x,
                o->calib_face.y,
                o->calib_face.width,
                o->calib_face.height,
                o->calib_face.x + o->calib_face.width / 2,
                o->calib_face.y + o->calib_face.height / 2);
            SetWindowTextW(w, b);
        }
        else if (o->calib_status == 0)
        {
            SetWindowTextW(w, L"~~Not Calibrated~~");
        }
        else if (o->calib_status == 1)
        {
            SetWindowTextW(w, L"Calibrating\nPlease stay still\n");
        }
    }
}