#include <stdio.h>
#include <NIDAQmx.h>
#include <stdlib.h>
#include <string.h>
#pragma comment (lib,"NIDAQmx.lib")

#include <math.h>
/*analog input mapping

----Right wheel----
Ai 1 (red)    Phase 1
Ai 9 (orange) Phase 2
Ai 2 (brown)  Sense

----Left wheel----
Ai 3  (red)    Phase 1
Ai 4  (orange) Phase 2
Ai 11 (brown)  Sense
----------------------------------

/*Pulses per rotation: ~654 pulses
*/


/*

Ch 55 - Dreapta
Ch 56 - Stanga
*/

/*  Adapted from the voltage example of the PCI6024e*/

int pci_6024_write_analog(void *pd, double left, double right);
int pci_6024_stop_task(void *pd);
typedef struct
{
    void *task;
    void *task_in;
    double samp_no;
    double rate;
    size_t ai_to_read;
}PCI6024_data;

void *pci_6024_init(void)
{
    PCI6024_data *pd = NULL;
    TaskHandle t1 = NULL;
    TaskHandle t2 = NULL;
    int ret1 = 0;
    int ret2 = 0;
    ret1 = DAQmxCreateTask("TaskOut", &t1);
    ret2 = DAQmxCreateTask("TaskIn",  &t2);

    if (!ret1 && !ret2)
    {
        pd = (PCI6024_data*)malloc(sizeof(PCI6024_data));
        memset(pd, 0, sizeof(PCI6024_data));
        DAQmxCfgOutputBuffer(t1, 0);
        DAQmxCfgInputBuffer(t2, 0);
        pd->task = t1;
        pd->task_in = t2;
        printf("Task created OK\n");
        return((void*)pd);
    }
    
    if (ret1 != 0)
    {
        DAQmxClearTask(t1); 
    }
    
    if (ret2 != 0)
    {
        DAQmxClearTask(t2);
    }
    return ((void*)pd);
}

int pci_6024_destroy(void **pd)
{
    if (pd == NULL || *pd == NULL)
    {
        return(-1);
    }
    else
    {
        PCI6024_data *lpd = *(PCI6024_data**)pd;
        pci_6024_stop_task(pd);
        DAQmxClearTask(lpd->task);
        DAQmxClearTask(lpd->task_in);
        free(*pd);
        *pd = NULL;
        return(0);
    }
}

int pci_6024_set_ao_chan(void *pd, char *chan)
{
    PCI6024_data *lpd = (PCI6024_data*)pd;
    if (pd == NULL)
    {
        return(-1);
    }

    int ret = DAQmxCreateAOVoltageChan(lpd->task, chan, "", -5.0, 5.0, DAQmx_Val_Volts, "");

    if (DAQmxFailed(ret) == 0)
    {
        printf("AO Chan set successfully\n");
    }
    else
    {
        printf("AO Chan set FAIL\n");
    }
    return(ret);
}


int pci_6024_set_ai_chan(void *pd, char *chan)
{
    PCI6024_data *lpd = (PCI6024_data*)pd;
    if (pd == NULL)
    {
        return(-1);
    }

    int ret = DAQmxCreateAIVoltageChan(lpd->task_in, chan, "", DAQmx_Val_RSE, -10.0, 10.0, DAQmx_Val_Volts, NULL);

        if (DAQmxFailed(ret) == 0)
        {
            lpd->ai_to_read++;
            printf("AI Chan set successfully\n");
        }
        else
        {
            char        errBuff[2048] = { '\0' };
            DAQmxGetExtendedErrorInfo(errBuff, 2048);
            printf("AI Chan set FAIL\n%s\n",errBuff);
        }
    return(ret);
}

int pci_6024_set_clk(void *pd, double rate, double samp_no)
{
    PCI6024_data *lpd = (PCI6024_data*)pd;
    int ret = -1;

    if (pd)
    {
        ret = DAQmxCfgSampClkTiming(lpd->task_in, "", (double)rate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, (uInt64)samp_no);
    }

    if (DAQmxFailed(ret) == 0)
    {
        printf("SampClkTiming set successfully\n");
        lpd->samp_no = samp_no;
        lpd->rate = rate;
    }
    else
    {
        printf("SampClkTiming set FAIL\n");
    }
    return(ret);
}


int pci_6024_start_task(void *pd)
{
    PCI6024_data *lpd = (PCI6024_data*)pd;
    if (pd == NULL)
    {
        return(-1);
    }
    int ret = DAQmxStartTask(lpd->task);
    DAQmxStartTask(lpd->task_in);
    if (DAQmxFailed(ret) == 0)
    {
        printf("Started task\n");
    }
    else
    {
        printf("Task Start FAIL\n");
    }
    return(ret);
}

int pci_6024_stop_task(void *pd)
{
    PCI6024_data *lpd = (PCI6024_data*)pd;
    double     data[2] = { 0.0,0.0 };

    if (pd == NULL)
    {
        return(-1);
    }

    DAQmxWriteAnalogF64(lpd->task, 1, 1, -1.0, DAQmx_Val_GroupByScanNumber, data, NULL, NULL);
    return(0);
}

int pci_6024_write_analog(void *pd, double left, double right)
{
    double     data[2] = { left,right };
    PCI6024_data *lpd = (PCI6024_data*)pd;
    if (pd == NULL)
    {
        return(-1);
    }
    int ret = DAQmxWriteAnalogF64(lpd->task, 1, 1, -1.0, DAQmx_Val_GroupByScanNumber, data, NULL, NULL);
    if (DAQmxFailed(ret) == 0)
    {

    }
    else
    {
        printf("WriteAnalog FAIL\n");
    }
    return(ret);
}


int pci_6024_read_analog(void *pd, size_t vec_in, size_t *vec_out, double *vec)
{
    PCI6024_data *lpd = (PCI6024_data*)pd;
    if (lpd&&vec_out&&vec&&vec_in>0)
    {
        DAQmxReadAnalogF64(lpd->task_in, (int)lpd->samp_no, -1.0, DAQmx_Val_GroupByScanNumber, vec, (int)lpd->samp_no*(int)lpd->ai_to_read, (int32*)vec_out, NULL);
    }
    return(0);
}
