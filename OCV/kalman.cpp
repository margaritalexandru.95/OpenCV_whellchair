#include <string.h>
#include <kalman.h>

#define COVARIANCE_PROCESS_NOISE                                (0.022f)     //Q (0.022f)
#define COVARIANCE_OBSERVATION_NOISE                            (0.1f)     //R (0.1f)
/*Kalman filter initializer*/
void kalman_init(kalman_data *kd)
{
    memset(kd, 0, sizeof(kalman_data));
    kd->Q = COVARIANCE_PROCESS_NOISE;
    kd->R = COVARIANCE_OBSERVATION_NOISE;
}

/*Kalman filter update routine*/
float kalman_update(kalman_data *kd, float val_in)
{
    float P_temp = kd->P + kd->Q;
    float x_temp_est = kd->x_est;
	
    kd->K = P_temp * (1.0f / (P_temp + kd->R));             //calculate the Kalman gain 
    kd->x_est = x_temp_est + kd->K * (val_in - x_temp_est); //calculate prediction
    kd->P = (1.0f - kd->K) * P_temp;                        //calculate covariance

    return (kd->x_est);
}
