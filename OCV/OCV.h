#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/face.hpp>
#include <Windows.h>
#include "resource.h"
#include <kalman.h>
using namespace cv;
using namespace std;
using namespace face;



#define CAMERA_TIMER WM_USER+0x1
#define FEEDER_TIMER CAMERA_TIMER+0x1
#define DEV_AO_LEFT                "Dev1/ao0"
#define DEV_AO_RIGHT               "Dev1/ao1"
#define LEFT_PH1                   "Dev1/ai3"
#define LEFT_PH2                   "Dev1/ai4"
#define RIGHT_PH1                  "Dev1/ai1"
#define RIGHT_PH2                  "Dev1/ai9"
#define AVG_CNT 15

#define clamp(x1,y,x2) ((MIN((x2),MAX((y),(x1)))))

typedef struct _ocv OCV;

typedef struct
{
    double voltage;
    double left;
    double right;
    int run;
    void *context;
    int stop;
    double kp;
    double ki;
    double kd;
    double kp_2;
    double ki_2;
    double kd_2;
    double *rv;
    double m_left;
    double m_right;
    double r_left;
    double r_right;
    double in_left;
    double in_right;
    double om_1;
    double om_2;
#ifdef USE_NETWORK
    double *raw_rv;
#endif
    size_t val_count;
    OCV *ocv;
}motor_data;

struct _ocv
{
	unsigned char go_to_origin;
    CRITICAL_SECTION cs;
    unsigned char straight;
    unsigned long long no_face_timeout;
    unsigned long long calib_timeout;
    motor_data md;
    unsigned char calib_status; //0 - not claribating , 1 - calibrating, 2 - calibrated
    Rect clf;   //calibration face
    Rect cle;   //calibration face
    VideoCapture *vc; //handle to camera
    /*Classifiers*/
    CascadeClassifier *face;
    CascadeClassifier *eye;
    double old_x_ratio;
    int has_face;

    int bmp_width;
    int bmp_height;
    unsigned char stopped;
    HDC dc;
    HBITMAP bmph;
    HBITMAP dc_bmp;
    void *px_data;
    Rect calib_face;
    Rect work_face;

    kalman_data xk;
    kalman_data yk;
    kalman_data lk;
    kalman_data rk;
    void *fm;
    void *mth;
    double new_kp;
    double new_ki;
    double new_kd;
    double new_kp_2;
    double new_ki_2;
    double new_kd_2;
    
#ifdef USE_NETWORK
    float refx;
    float refy;
    float cx;
    float cy;
#endif
};

int pci_6024_destroy(void **pd);
void *pci_6024_init(void);
int pci_6024_set_ao_chan(void *pd, char *chan);
int pci_6024_start_task(void *pd);
int pci_6024_write_analog(void *pd, double val1, double val2);
int pci_6024_stop_task(void *pd);
int pci_6024_read_analog(void *pd, size_t vec_in, size_t *vec_out, double *vec);
int pci_6024_set_clk(void *pd, double rate, double samp_no);
int pci_6024_set_ai_chan(void *pd, char *chan);
int robot_feeder(HWND win, unsigned int msg, unsigned int time);
void frame_provider(OCV *o, HWND win);
void init_ocv(OCV *o);