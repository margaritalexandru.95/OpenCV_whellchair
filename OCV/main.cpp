#include <string>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "OCV.h"
#include <Commctrl.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

extern DWORD motor_main(void *pv);
extern DWORD network_main(void *pv);
typedef struct
{
    float cx[2];
    float cy[2];
    float ca[2];
    float left[2];
    float right[2];
    char good;
} OCV_SHARED;

#define TOLERANCE 0.05f

static void robot_feeder_stop(OCV *o)
{
    if (o->mth != NULL)
    {
        EnterCriticalSection(&o->cs);
        o->md.run = 0;
        o->straight = 0;
        LeaveCriticalSection(&o->cs);
        WaitForSingleObject(o->mth, -1);
        CloseHandle(o->mth);
        o->mth = NULL;

    }

    o->calib_timeout = 0;
	o->calib_status = 0;
}

/*How the wheelchair control works (or should work):
* Head up the wheelchair goes forward
* Head left/right (while up), the wheelchair goes left/right
*
*       +-------------------+ ----->
*       |                   |
*       |  Face rectangle   |       h
*       |                   |       e
*       |      (center)     |       i
*       |         +         |       g
*       |       (x,y)       |       h
*       |                   |       t
*       |                   |
*       +-------------------+------>
*       |                   |
*       |      width        |
*       |<----------------->|
*
* To determine the position of the subject's head the following algorithm is used:
* 1) We store an initial set of coorindates (x,y) in refx, refy during the calibration stage
* 2) For each call of the robot_feeder() the following actions happen form a control-perspective:
*    2.1) A valid frame is caputred
*    2.2) x,y,area coordinates are stored in cx,cy,ca variables
*    2.3)  Filter the input values from OpenCV using Kalman filtering
*    2.4) For each component (x,y,area) a ratio is calculated:
*         2.3.1) If cx/refx < 0.95 the wheelchair will go left, cx/refx > 1.05 - the wheelchair will go right
*         2.3.2) If cy/refy < 0.95 the wheelchair will go forward.
*          The 0.10 tolerance has been choosen to avoid unexpected commands to be sent to the wheelchair.
*          as the face rectangle does not have a fixed position even if the user stays still.
*     2.5) Apply some boundings to the final values to make sure everything is in some safe values
*     2.6) Calculate the final command values
*     2.7 Apply Kalman filtering again on the command values.
*     2.8) Send the command values to the underlying bridge layer (PCI6024e)

*     NOTE: Going backward without additional sensors is dangerous and because of this it was not implemented.
*/


/*
*This routine is called each ~33 ms (~30 FPS) to obtain a frame and generate a command for the wheelchair
* Think of it as the heart and brain of the application
*/

static int robot_feeder(HWND win, unsigned int msg, unsigned int time)
{

    HWND w = NULL;
    OCV *o = (OCV*)GetWindowLongPtrW((HWND)win, GWLP_USERDATA);
    
    DWORD dummy = 0;
    float left = 0.0;
    float right = 0.0;
    float xr = 0.0;
    float yr = 0.0;
    float refx = 0.0;
    float refy = 0.0;
    float cx = 0.0;
    float cy = 0.0;

    if (o->vc)
        frame_provider(o, win);

    if (o == NULL)
    {
        return(0);
    }

    refx = (float)((float)o->calib_face.width / 2.0 + o->calib_face.x);    //calculate the X coordinate of the reference point
    refy = (float)((float)o->calib_face.height / 2.0 + o->calib_face.y);   //calculate the Y coordinate of the reference point
    cx = (float)(o->work_face.x + (float)o->work_face.width / 2.0);        //calculate the X coordinate of the work point
    cy = (float)(o->work_face.y + (float)o->work_face.height / 2.0);    //calculate the Y coordinate of the work point

    /*Robot is not running*/
    if (o->md.run == 0)
    {
        /*Make sure the kalman filter gets constantly fed with data
		since it must 'be aware' of the current values in the system*/
        cx = kalman_update(&o->xk, cx);
        cy = kalman_update(&o->yk, cy);
        left = kalman_update(&o->lk, left);
        right = kalman_update(&o->rk, right);
        return(0);
    }

    /*Invalid calibration data - this is mostly because the face disappeared*/
    if (o->calib_status < 3)
    {
        robot_feeder_stop(o);
    }

	/*Get raw ratios for comparison*/
	xr = (refx > 0.0f && cx >= 0.0f) ? (cx / refx) : 0.0f;
	yr = (refy > 0.0f && cy >= 0.0f) ? (cy / refy) : 0.0f;
    
	/*
	*Instead of recalibrating each time the face is no longer detected 
	*we should just stop the wheelchair and then just wait 
	*for the user to go back into the calibration point
	*/

    if (o->work_face.width == 0 || o->work_face.height == 0)
    {
		o->go_to_origin = 1;
    }
	else if (yr >= 1.0f - TOLERANCE && 
		     yr <= 1.0f + TOLERANCE && 
		     xr >= 1.0f - TOLERANCE && 
		     xr <= 1.0f + TOLERANCE && 
		     o->go_to_origin)
	{
		o->go_to_origin = 0;
	}

	/*Filter the coordinates*/
	cx = kalman_update(&o->xk, cx);
	cy = kalman_update(&o->yk, cy);

	/*Calculate the ratios*/
	

	if (o->go_to_origin)
	{
		yr = 1.0;
		xr = 1.0;
	}
	else
	{
		xr = (refx > 0.0f && cx >= 0.0f) ? (cx / refx) : 0.0f;
		yr = (refy > 0.0f && cy >= 0.0f) ? (cy / refy) : 0.0f;
	}
	
    /*Calculate forward speed*/
    if (yr > TOLERANCE && yr < 1.0f - TOLERANCE)
    {
        right = left = 1.0f - (yr + TOLERANCE);
    }
    else
    {
		right = left =  yr = 0.0;
    }

    /*Calculate steering*/
    if (xr > TOLERANCE && xr < 1.0f - TOLERANCE)
    {
        xr = 1.0f - (xr + TOLERANCE);
        right = clamp(0.0f, right - (right * xr * 10.0f), right);
    }
    else if (xr > 1.0f + TOLERANCE)
    {
        xr = (xr - TOLERANCE) - 1.0f;
        left = clamp(0.0f, left - (xr * left * 10.0f), left);

    }

	/*Clamp the commands*/
    left = clamp(0.0f, left * 10.0f, 1.0f);
    right = clamp(0.0f, right * 10.0f, 1.0f);

    /*Filter the commands*/
    left = kalman_update(&o->lk, left);
    right = kalman_update(&o->rk, right);

	/*Try to send the command*/
    if (GetExitCodeThread(o->mth, &dummy) && dummy == STILL_ACTIVE)
    {
        EnterCriticalSection(&o->cs);
#ifdef USE_NETWORK
        o->refx = refx;
        o->refy = refy;
        o->cx = cx;
        o->cy = cy;
#endif
		/*
		*Additional command processing could 
		*be done here before sending the data 
		*to the motor thread
		*/
        o->md.left = (o->straight ? 1.0 : left);
        o->md.right = (o->straight ? 1.0 : right);
        LeaveCriticalSection(&o->cs);
    }

	if ((w = GetDlgItem(win, IDC_ARIA_DATA)) != NULL)
	{
		wchar_t b[256] = { 0 };
		_snwprintf(b, 256, L"Robot:\nSetVel(%.2lf, %.2lf)\nXR = %.2lf YR = %.2lf\n", left, right, xr, yr);
		SetWindowTextW(w, b);
	}

    return(0);
}


static void update_edit_boxes(HWND win, OCV *o)
{
    char buf[256] = { 0 };

    snprintf(buf, 256, "%.3lf", o->new_kp);
    SetWindowTextA(GetDlgItem(win, IDC_EDIT2), buf);

    snprintf(buf, 256, "%.3lf", o->new_ki);
    SetWindowTextA(GetDlgItem(win, IDC_EDIT3), buf);

    snprintf(buf, 256, "%.3lf", o->new_kd);
    SetWindowTextA(GetDlgItem(win, IDC_EDIT4), buf);
    /*PID Right*/
    snprintf(buf, 256, "%.3lf", o->new_kp_2);
    SetWindowTextA(GetDlgItem(win, IDC_EDIT5), buf);

    snprintf(buf, 256, "%.3lf", o->new_ki_2);
    SetWindowTextA(GetDlgItem(win, IDC_EDIT6), buf);

    snprintf(buf, 256, "%.3lf", o->new_kd_2);
    SetWindowTextA(GetDlgItem(win, IDC_EDIT7), buf);
}

static void update_static_boxes(HWND win, OCV *o)
{
    char buf[256] = { 0 };

    snprintf(buf, 256, "%.3lf", o->md.kp);
    SetWindowTextA(GetDlgItem(win, IDC_KP), buf);

    snprintf(buf, 256, "%.3lf", o->md.ki);
    SetWindowTextA(GetDlgItem(win, IDC_KI), buf);

    snprintf(buf, 256, "%.3lf", o->md.kd);
    SetWindowTextA(GetDlgItem(win, IDC_KD), buf);

    /*PID Right*/
    snprintf(buf, 256, "%.3lf", o->md.kp_2);
    SetWindowTextA(GetDlgItem(win, IDC_KP2), buf);

    snprintf(buf, 256, "%.3lf", o->md.ki_2);
    SetWindowTextA(GetDlgItem(win, IDC_KI2), buf);

    snprintf(buf, 256, "%.3lf", o->md.kd_2);
    SetWindowTextA(GetDlgItem(win, IDC_KD2), buf);

    /*Voltage Slider*/
    snprintf(buf, 256, "Voltage %.3lf", o->md.voltage);
    SetWindowTextA(GetDlgItem(win, IDC_STATIC_VOLTS), buf);

}


static void read_settings(HWND win, OCV *o)
{
    double kp = 0.0;
    double ki = 0.0;
    double kd = 0.0;
    double kp_2 = 0.0;
    double ki_2 = 0.0;
    double kd_2 = 0.0;
    FILE *f = fopen("settings.txt", "r");
    if (f)
    {
        if (fscanf(f, "%lf,%lf,%lf;%lf,%lf,%lf", &kp, &ki, &kd, &kp_2, &ki_2, &kd_2) >= 6)
        {
            /*PID Left*/
            o->new_kd = kd;
            o->new_kp = kp;
            o->new_ki = ki;

            /*PID Right*/
            o->new_kd_2 = kd_2;
            o->new_kp_2 = kp_2;
            o->new_ki_2 = ki_2;

        }
        fclose(f);
    }
    SendMessage(win, WM_COMMAND, IDC_BUTTON4, 0); //mimick that we press 'Tweak'
}

static void write_settings(HWND win, OCV *o)
{
    FILE *f = fopen("settings.txt", "w");
    if (f)
    {
        fprintf(f, "%.3lf, %.3lf, %.3lf;%.3lf, %.3lf, %.3lf", o->new_kp, o->new_ki, o->new_kd, o->new_kp_2, o->new_ki_2, o->new_kd_2);
        fclose(f);
    }
}

/*
* Callback routine for message handling of the application's main window
* Here are handled the buttons of the window
*/

static INT_PTR __stdcall dlg_proc(HWND win, unsigned int msg, WPARAM wpm, LPARAM lpm)
{
    OCV *o = (OCV*)GetWindowLongPtrW((HWND)win, GWLP_USERDATA);
    if (o == NULL)
    {
        HWND parent = GetParent(win);
        o = (OCV*)GetWindowLongPtrW((HWND)parent, GWLP_USERDATA);
    }

    switch (msg)
    {

    case WM_NOTIFY:
    {
        NMHDR *hdr = (NMHDR*)lpm;
        if (hdr->code == UDN_DELTAPOS)
        {
            NMUPDOWN *nud = (NMUPDOWN*)lpm;


            /*KP tweak*/
            if (hdr->idFrom == IDC_SPIN3)
            {
                o->new_kp += -(double)nud->iDelta / 1000.0;
                if (o->new_kp < 0.0)
                    o->new_kp = 0.0;
            }
            /*KI tweak*/
            else if (hdr->idFrom == IDC_SPIN4)
            {
                o->new_ki += -(double)nud->iDelta / 1000.0;
                if (o->new_ki < 0.0)
                    o->new_ki = 0.0;
            }
            /*KD tweak*/
            else if (hdr->idFrom == IDC_SPIN5)
            {
                o->new_kd += -(double)nud->iDelta / 1000.0;
                if (o->new_kd < 0.0)
                    o->new_kd = 0.0;

            }

            /*PID Right*/
            /*KP tweak*/
            else if (hdr->idFrom == IDC_SPIN6)
            {
                o->new_kp_2 += -(double)nud->iDelta / 1000.0;
                if (o->new_kp_2 < 0.0)
                    o->new_kp_2 = 0.0;
            }
            /*KI tweak*/
            else if (hdr->idFrom == IDC_SPIN7)
            {
                o->new_ki_2 += -(double)nud->iDelta / 1000.0;
                if (o->new_ki_2 < 0.0)
                    o->new_ki_2 = 0.0;
            }
            /*KD tweak*/
            else if (hdr->idFrom == IDC_SPIN8)
            {
                o->new_kd_2 += -(double)nud->iDelta / 1000.0;
                if (o->new_kd_2 < 0.0)
                    o->new_kd_2 = 0.0;

            }
            update_edit_boxes(win, o);
        }
        break;
    }

    case WM_COMMAND:
    {

        if (wpm == IDC_BUTTON2 || wpm == IDC_BUTTON1)
        {
            if (o->calib_status == 3)
            {
                o->md.run = 1;

                if (o->mth == NULL)
                {
#if 0
                    if (wpm == IDC_BUTTON1)
                        o->straight = 1;
                    else
                        o->straight = 0;
#endif
                    o->mth = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)motor_main, (void*)&o->md, 0, NULL);
                }
                if (o->mth == NULL)
                {
                    o->straight = 0;
                    o->md.run = 0;
                }
            }
        }
        else if (wpm == IDC_BUTTON3)
        {
            robot_feeder_stop(o);
        }
        else if (wpm == IDC_BUTTON4 || wpm == IDC_BUTTON5)
        {
            EnterCriticalSection(&o->cs);
            o->new_kp = o->md.kp = (wpm == IDC_BUTTON4) ? o->new_kp : 0.0;
            o->new_ki = o->md.ki = (wpm == IDC_BUTTON4) ? o->new_ki : 0.0;
            o->new_kd = o->md.kd = (wpm == IDC_BUTTON4) ? o->new_kd : 0.0;
            o->new_kp_2 = o->md.kp_2 = (wpm == IDC_BUTTON4) ? o->new_kp_2 : 0.0;
            o->new_ki_2 = o->md.ki_2 = (wpm == IDC_BUTTON4) ? o->new_ki_2 : 0.0;
            o->new_kd_2 = o->md.kd_2 = (wpm == IDC_BUTTON4) ? o->new_kd_2 : 0.0;
            LeaveCriticalSection(&o->cs);

            update_static_boxes(win, o);
            update_edit_boxes(win, o);
        }
        else if (HIWORD(wpm) == EN_CHANGE)
        {
            if (LOWORD(wpm) == IDC_EDIT2)
            {
                char buf[256] = { 0 };
                GetWindowTextA(GetDlgItem(win, LOWORD(wpm)), buf, 255);
                o->new_kp = strtod(buf, NULL);
            }
            else if (LOWORD(wpm) == IDC_EDIT3)
            {
                char buf[256] = { 0 };
                GetWindowTextA(GetDlgItem(win, LOWORD(wpm)), buf, 255);
                o->new_ki = strtod(buf, NULL);
            }
            else if (LOWORD(wpm) == IDC_EDIT4)
            {
                char buf[256] = { 0 };
                GetWindowTextA(GetDlgItem(win, LOWORD(wpm)), buf, 255);
                o->new_kd = strtod(buf, NULL);
            }
            else if (LOWORD(wpm) == IDC_EDIT5)
            {
                char buf[256] = { 0 };
                GetWindowTextA(GetDlgItem(win, LOWORD(wpm)), buf, 255);
                o->new_kp_2 = strtod(buf, NULL);
            }
            else if (LOWORD(wpm) == IDC_EDIT6)
            {
                char buf[256] = { 0 };
                GetWindowTextA(GetDlgItem(win, LOWORD(wpm)), buf, 255);
                o->new_ki_2 = strtod(buf, NULL);
            }
            else if (LOWORD(wpm) == IDC_EDIT7)
            {
                char buf[256] = { 0 };
                GetWindowTextA(GetDlgItem(win, LOWORD(wpm)), buf, 255);
                o->new_kd_2 = strtod(buf, NULL);
            }
        }
        else if (wpm == IDC_BUTTON6)
        {
            read_settings(win, o);
        }
        else if (wpm == IDC_BUTTON7)
        {
            write_settings(win, o);
        }
        break;
    }
    case WM_HSCROLL:
    {
		
        if ((HWND)lpm == GetDlgItem(win, IDC_SLIDER3))
        {
            if (LOWORD(wpm) == TB_THUMBPOSITION || 
				LOWORD(wpm) == TB_THUMBTRACK    || 
				LOWORD(wpm) == TB_PAGEDOWN      || 
				LOWORD(wpm) == TB_PAGEUP        ||
				LOWORD(wpm) == TB_LINEUP        ||
				LOWORD(wpm) == TB_LINEDOWN)
            {
                EnterCriticalSection(&o->cs);

                if (LOWORD(wpm) == TB_THUMBPOSITION || LOWORD(wpm) == TB_THUMBTRACK)
                {
                    o->md.voltage = (double)HIWORD(wpm) / 100.0 * 4.0;
                }
                else
                {
                    unsigned int pos = (unsigned int)SendMessage((HWND)lpm, TBM_GETPOS, 0, 0);
                    o->md.voltage = (double)pos / 100.0 * 4.0;
                }
                LeaveCriticalSection(&o->cs);

            }
            update_static_boxes(win, o);
        }
        break;
    }

    case WM_CLOSE:
        robot_feeder_stop(o);
        if (o->vc)
            o->vc->release();
        PostQuitMessage(0);
        return(0);
    }

    return(0);
}

/*workflow diagram:
* +------------------+    +---------------+    +----------------------------+    +----------------------+
* | signal from timer| -> | Capture frame | -> | Check frame for valid face | -> | Extract coodrindates |---+
* +------------------+    +---------------+    +----------------------------+    +----------------------+   |
*                                        +----------------------------------+    +----------------------+   |
*                                        | Send command to underlying layers| <- |  Compute coordinates |<--+
*                                        +----------------------------------+    +----------------------+
*/



int main(int argc, char **argv)
{
    OCV o;
    MSG m = { 0 };
    memset(&o, 0, sizeof(OCV)); //clear the memory to make sure everything it's nice and tidy
    InitializeCriticalSection(&o.cs);
    o.md.ocv = &o;
#ifndef NDEBUG
    ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif
    kalman_init(&o.xk); //initialize kalman for x    input
    kalman_init(&o.yk); //initialize kalman for y    input
    kalman_init(&o.lk); //initialize kalman for left  wheel command
    kalman_init(&o.rk); //initialize kalman for right wheel command


    HWND w = CreateDialogW(NULL, MAKEINTRESOURCEW(IDD_FORMVIEW), NULL, (DLGPROC)dlg_proc); //create the window
    SendMessage(GetDlgItem(w, IDC_SLIDER3), TBM_SETPAGESIZE, 0, 10);
    SetWindowLongPtrW(w, GWLP_USERDATA, (LONG_PTR)&o);
    read_settings(w, &o);
    init_ocv(&o);

    SetTimer(w, CAMERA_TIMER, 33, (TIMERPROC)robot_feeder); //make the application tick
#ifdef USE_NETWORK
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)network_main, (void*)&o, 0, NULL);
#endif
    while (GetMessageW(&m, NULL, 0, 0))
    {
        DispatchMessage(&m);
        TranslateMessage(&m);
    }

    return(0);
}
