#include <stdio.h>
#include <PID.h>
#include <OCV.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>

/* The motors are handled under a speparate thread
* because the timing is very important and the data must be continously fed from/to the acquisition card
* The reason for this is that it has been observed that the PID is sensitive to the sampling rate
* A big sampling rate will result in a better control of the motors
*/
#define USE_BRIDGE
#define RATE 10000
#define READ 1000
#define omega(x) ((((x)*(2*M_PI))/654.0)/0.1)
#define mps(x,r) ((omega(x))*(r))
#define VOLTAGE_TO_SPEED(v,r) ((v)*(r))
#define SPEED_TO_VOLTAGE(s,r) ((s)/(r))
#define WHEEL_VALUE(w,p,i) (((i)*4+((w)*2)+(p)))
#define VOLTAGE_SPEED_RATIO 2.2097

/*
KP 0.5
KI 2.1
*/


/*input - vector mapping - when connected

0 - right wheel, phase 1
1 - left  wheel, phase 1
2 - left  wheel, phase 2
3 - right wheel, phase 2

 Convention:
 LEFT   =  0
 RIGHT  =  1
 PHASE1 =  0
 PHASE2 =  1

Pulses per rotation: ~654 pulses
*/


/*Some notes about how this should work
1) The OCV will pass ratios for the left wheel and right wheel
2) The ratios must be applied to the the maximum advisable speed of the wheelchair to get the actual requested speed
3) The computed speed at step 2 is then used as a reference for the PID controller
4) The actual speed is read from the acquisition card, converted from pulses into speed and then used to close the control loop
5) The PID will return a new speed value that must be used to further compute the speed
6) To send the command to the motors, there must be a link between the reference value that enters in the PID, the computed output and the voltage that is going to be applied to the motors
    A way to link this is:
    (reference/input)*(maximum_voltage_allowed*)
    *for safety, the maximum will be set to 2 volts
*/

static int check_movement(double *vec, size_t vec_len, int *sw, int *ew)
{
    *sw = 2;
    *ew = 0;

    int diff = 0;
    for (int w = 0; w < 2; w++)
    {
        double lv = -1.0;
        for (size_t i = 0; i < vec_len; i++)
        {

            if (lv != -1.0 && vec[WHEEL_VALUE(w, 0, i)] != lv)
            {
                if (w == 0)
                {
                    *sw = 0;
                    *ew = 1;
                }
                if (w == 1)
                {
                    if ((*sw) == 2)
                        *sw = 1;
                    *ew = 2;
                }
                break;
            }

            if (vec[WHEEL_VALUE(w, 0, i)] != lv)
                lv = vec[WHEEL_VALUE(w, 0, i)];

        }
    }
    return(sw <= ew);
}

static int check_wheels(double *vec, size_t vec_len, int *dir1, int *dir2, size_t *pw1, size_t *pw2)
{
    int sw = 0;
    int ew = 0;
    *pw1 = 0;
    *pw2 = 0;


    if (!check_movement(vec, vec_len, &sw, &ew))
    {
        return(-1);
    }

    for (int w = sw; w < ew; w++)
    {
        unsigned char v1u = 0;
        unsigned char v2u = 0;
        size_t v1p = 0;
        size_t v2p = 0;
        int *dir = (w == 0 ? dir1 : dir2);
        size_t *pw = (w == 0 ? pw1 : pw2);
        *dir = 0;

        for (size_t i = 0; i < vec_len; i++)
        {
            if (*dir == 0 && vec[WHEEL_VALUE(w, 0, i)] == vec[WHEEL_VALUE(w, 1, i)] && vec[WHEEL_VALUE(w, 0, i)] > 0.0) //find the positive fronts
            {
                size_t v1l = 0;
                size_t v2l = 0;

                /*Here will find what signal falls first
                * If phase1 falls first, then the wheel is going forward
                * If phase2 falls first, then the wheel is going backward,
                * Simple, no?
                */
                for (size_t j = i; j < vec_len; j++)
                {
                    if (vec[WHEEL_VALUE(w, 0, j)] < 10.0 && v1l == 0) //get phase 1
                        v1l = j;

                    if (vec[WHEEL_VALUE(w, 1, j)] < 10.0 && v2l == 0) //get phase 2
                        v2l = j;

                    if (v1l != 0 && v2l != 0)
                    {
                        if (v1l < v2l)
                            *dir = 1;
                        else if (v1l > v2l)
                            *dir = -1;
                        break;
                    }
                }
            }

            /*Check the rising fronts to calculate the wheel rotation*/

            /*Check the fronts on phase 1*/

            if (vec[WHEEL_VALUE(w, 0, i)] == 10.0 && !v1u)
            {
                v1p++;
                v1u = 1;
            }
            else if (vec[WHEEL_VALUE(w, 0, i)] == 0.0)
                v1u = 0;


            /*Check the fronts on phase 2*/

            if (vec[WHEEL_VALUE(w, 1, i)] == 10.0 && !v2u)
            {
                v2p++;
                v2u = 1;
            }
            else if (vec[WHEEL_VALUE(w, 1, i)] == 0.0)
                v2u = 0;
        }

        if (*dir != 0)
        {
            *pw = v1p > v2p ? v1p : v2p;
        }
    }
    return(0); /*Get the maximum number of pulses*/
}



DWORD motor_main(void *pv)
{
    int running = 1; /*assume we are running - this will be modified later*/
    int stopped = 0;
    double pid_rate = (double)READ / (double)RATE;
    size_t val_out = 0;
    motor_data *md = (motor_data*)pv;
    PID_State l_pid = { 0 };
    PID_State r_pid = { 0 };
	EnterCriticalSection(&md->ocv->cs);
    md->val_count = READ;

    md->rv = (double*)calloc(md->val_count * 4, sizeof(double));
#ifdef USE_NETWORK
    md->raw_rv = (double*)calloc(md->val_count * 4, sizeof(double));
#endif
	LeaveCriticalSection(&md->ocv->cs);
    pid_init(&l_pid, 0.0, 0.0, 0.0, pid_rate, &md->in_left, &md->r_left);
    pid_init(&r_pid, 0.0, 0.0, 0.0, pid_rate, &md->in_right, &md->r_right);

    /*set the PID bounds to avoid surprises like the output being always 0 - 20180425*/



    /*Intialize the bridge*/
    EnterCriticalSection(&md->ocv->cs);
    int failed = 0;
#ifdef USE_BRIDGE
    md->context = pci_6024_init();

    if (md->context == NULL)
    {
        md->run = 0;
        LeaveCriticalSection(&md->ocv->cs);
        ExitThread(-1);
    }
    else
    {
        pci_6024_set_ao_chan(md->context, DEV_AO_LEFT);
        pci_6024_set_ao_chan(md->context, DEV_AO_RIGHT);
        pci_6024_set_ai_chan(md->context, LEFT_PH1);
        pci_6024_set_ai_chan(md->context, LEFT_PH2);
        pci_6024_set_ai_chan(md->context, RIGHT_PH1);
        pci_6024_set_ai_chan(md->context, RIGHT_PH2);
        pci_6024_set_clk(md->context, RATE, READ);
        pci_6024_write_analog(md->context, 0.0, 0.0);
    }
#endif
    LeaveCriticalSection(&md->ocv->cs);


   

    while (running)
    {
        size_t pw1 = 0;
        size_t pw2 = 0;
        int dw1 = 0;
        int dw2 = 0;

        val_out = 0;

        //stopped = 0;
        /*Step 1*/
        EnterCriticalSection(&md->ocv->cs);
        double max_voltage = md->voltage + 0.25 * md->voltage;
        max_voltage = clamp(md->voltage, max_voltage, 5.0);
        md->r_left = md->left * md->voltage;
        md->r_right = md->right * md->voltage;
        running = md->run;
        /*we may attempt to tweak the PID on the fly using pid_adjust()*/
        pid_adjust(&l_pid, md->kp, md->ki, md->kd, pid_rate);
        pid_adjust(&r_pid, md->kp_2, md->ki_2, md->kd_2, pid_rate);
        pid_bounds(&l_pid, 0.0, max_voltage);
        pid_bounds(&r_pid, 0.0, max_voltage);
        if (running == 0)
        {
            md->r_left = 0.0;
            md->r_right = 0.0;
        }
       

#ifdef USE_BRIDGE
        pci_6024_read_analog(md->context, md->val_count * 4, &val_out, md->rv);

#endif
#ifdef USE_NETWORK
        memcpy(md->raw_rv, md->rv, md->val_count * 4 * sizeof(double));
#endif
        for (size_t i = 0; i < md->val_count * 4; i++)
        {
            /*Cut out the signals that are out of the acceptable values [0 10] volts*/
            if (md->rv[i] > 9.0)
            {
                md->rv[i] = 10.0;
            }
            else
            {
                md->rv[i] = 0.0;
            }
        }
        check_wheels(md->rv, md->val_count, &dw1, &dw2, &pw1, &pw2);
        md->om_1 = omega((double)pw1);
        md->om_2 = omega((double)pw2);


        /*Convert the speed into voltage based on the speed/voltage ratio*/
        md->in_left = SPEED_TO_VOLTAGE(md->om_1, VOLTAGE_SPEED_RATIO);
        md->in_right = SPEED_TO_VOLTAGE(md->om_2, VOLTAGE_SPEED_RATIO);

        /*update the PID for each wheel*/
        md->m_left = pid_update(&l_pid);
        md->m_right = pid_update(&r_pid);
        LeaveCriticalSection(&md->ocv->cs);



        /*Send the command to the acquisition card*/
#ifdef USE_BRIDGE
        pci_6024_write_analog(md->context, md->m_left, md->m_right);
#else
        Sleep(100);
#endif

        if (md->om_1 == 0.0 && md->om_2 == 0.0)
        {
            stopped++;
        }
        else
        {
            stopped = 0;
        }
    }

#ifdef USE_BRIDGE
        pci_6024_write_analog(md->context, 0.0, 0.0);
#endif

#ifdef USE_BRIDGE
    pci_6024_destroy(&md->context);
#endif
	EnterCriticalSection(&md->ocv->cs);
    free(md->rv);
    md->rv = NULL;
#ifdef USE_NETWORK
    free(md->raw_rv);
    md->raw_rv = NULL;
#endif
	LeaveCriticalSection(&md->ocv->cs);
    ExitThread(0);
}
