
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <windows.h>
#include <Ws2def.h>
#include <Ws2tcpip.h>
#include <OCV.h>
#ifdef USE_NETWORK
#define DATA_PHASE 0x1
#define DATA_PHASE_RAW 0x2
#define DATA_FACE_COORD_REF 0x3
#define DATA_FACE_COORD_CURRENT 0x4
#define DATA_FACE_CMD_RATIO_RAW 0x5
#define DATA_FACE_CMD_FINAL 0x6
#define DATA_MOTOR_COMMAND_REF 0x7
#define DATA_MOTOR_MAX_VOLT 0x8
#define DATA_MOTOR_OMEGA 0x9
#define DATA_MOTOR_COMMAND_FINAL 0xA
#define DATA_MOTOR_OMEGA_TO_VOLT 0xB
#define STATUS_RECEIVED 0xfe
#pragma comment(lib,"Ws2_32.lib")

DWORD network_main(void *pv)
{
    OCV *o = (OCV*)pv;
    unsigned char cmd = 0;
    int status = 0;
    WSADATA wsaData = { 0 };
    SOCKET sock = 0;
    SOCKET conn = 0;
	ADDRINFO *out=NULL;
	ADDRINFO hints;
	memset(&hints, 0, sizeof(ADDRINFO));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	
	
	
    WSAStartup(MAKEWORD(2, 2), &wsaData);		   //initialize the library
	status = getaddrinfo(NULL, "2712", &hints, &out);
	sock = socket(out->ai_family, out->ai_socktype, out->ai_protocol);					//create the socket
   
	if (sock != INVALID_SOCKET)
	{
		status = bind(sock, (const struct sockaddr*)out->ai_addr, (int)out->ai_addrlen);  //bind the address to the socket
	}
	freeaddrinfo(out);

    /*enter the main loop to handle the requests*/


	while (sock!=INVALID_SOCKET)
	{
		status = listen(sock, 1);										//put the socket in "Listen Mode"
		conn = accept(sock, NULL, 0); //wait for the client to connect

		if (conn == INVALID_SOCKET)
		{
			Sleep(10);
			continue;
		}

		while (1)
		{

			unsigned char req[] =
			{
				DATA_FACE_COORD_REF,
				DATA_FACE_COORD_CURRENT,
				DATA_FACE_CMD_RATIO_RAW,
				DATA_FACE_CMD_FINAL ,
				DATA_MOTOR_COMMAND_REF,
				DATA_MOTOR_MAX_VOLT ,
				DATA_MOTOR_OMEGA ,
				DATA_MOTOR_COMMAND_FINAL,
				DATA_MOTOR_OMEGA_TO_VOLT
			};

			for (int i = 0; i < sizeof(req) / sizeof(char); i++)
			{
				double *send_buf = NULL;
				size_t len = 0;

				printf("REcv 1 done\n");
				

				EnterCriticalSection(&o->cs);
				switch (req[i])
				{
				case DATA_PHASE:
					if (o->md.rv)
					{
						len = o->md.val_count * sizeof(double) * 4;
						send_buf = (double*)calloc(len, 1);
						memcpy(send_buf, o->md.rv, len);
					}
					else
					{
						len = 1000 * 4 * sizeof(double);
						send_buf = (double*)calloc(1000 * 4, sizeof(double));
					}
					break;
				case DATA_PHASE_RAW:
					if (o->md.raw_rv)
					{
						len = o->md.val_count * sizeof(double) * 4;
						send_buf = (double*)calloc(len, 1);
						memcpy(send_buf, o->md.raw_rv, len);
					}
					else
					{
						len = 1000 * 4 * sizeof(double);
						send_buf = (double*)calloc(1000 * 4, sizeof(double));
					}
					break;
				case DATA_FACE_CMD_RATIO_RAW:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = o->md.left;
					send_buf[1] = o->md.right;
					break;
				case DATA_FACE_COORD_CURRENT:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->cx;
					send_buf[1] = (double)o->cy;
					break;
				case DATA_FACE_COORD_REF:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->refx;
					send_buf[1] = (double)o->refy;
					break;
				case DATA_MOTOR_COMMAND_REF:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->md.r_left;
					send_buf[1] = (double)o->md.r_right;
					break;
				case DATA_MOTOR_MAX_VOLT:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->md.voltage;
					send_buf[1] = (double)o->md.voltage;
					break;
				case DATA_MOTOR_OMEGA:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->md.om_1;
					send_buf[1] = (double)o->md.om_2;
					break;
				case DATA_MOTOR_COMMAND_FINAL:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->md.m_left;
					send_buf[1] = (double)o->md.m_right;
					break;
				case DATA_MOTOR_OMEGA_TO_VOLT:
					len = 2 * sizeof(double);
					send_buf = (double*)calloc(2, sizeof(double));
					send_buf[0] = (double)o->md.in_left;
					send_buf[1] = (double)o->md.in_right;
					break;
				}
				LeaveCriticalSection(&o->cs);

				if (send_buf)
				{
					send(conn, (const char*)send_buf, (int)len, 0);
					free(send_buf);
				}
				else
				{
					printf("Send done\n");

					printf("Recv done %d\n", status);
				}

			}
		}
		shutdown(conn, SD_BOTH);
		closesocket(conn);
	}

	if (sock != INVALID_SOCKET)
	{
		shutdown(conn, SD_BOTH);
		closesocket(sock);
	}

	WSACleanup();
	return(0);
}
#endif