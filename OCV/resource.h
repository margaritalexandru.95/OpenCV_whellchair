//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OCV.rc
//
#define IDD_FORMVIEW                    101
#define IDC_BUTTON1                     1001
#define IDC_STRAIGHT                    1001
#define IDC_BUTTON                      1001
#define IDC_BUTTON2                     1002
#define IDC_BUTTON3                     1003
#define IDC_CAMERA                      1005
#define IDC_CAMERA_FRAME                1006
#define IDC_CALIB_SHOW                  1007
#define IDC_ARIA_DATA                   1008
#define IDC_CAMERA2                     1009
#define IDC_CAMERA_FRAME2               1010
#define IDC_CAMERA3                     1011
#define IDC_CAMERA_FRAME3               1012
#define IDC_RADIO1                      1013
#define IDC_RADIO2                      1014
#define IDC_RADIO3                      1015
#define IDC_RADIO4                      1016
#define IDC_CAMERA4                     1017
#define IDC_CAMERA_FRAME4               1018
#define IDC_EDIT2                       1022
#define IDC_EDIT3                       1023
#define IDC_SPIN3                       1024
#define IDC_SPIN4                       1025
#define IDC_BUTTON4                     1026
#define IDC_BUTTON5                     1027
#define IDC_EDIT4                       1028
#define IDC_SPIN5                       1029
#define IDC_KP                          1030
#define IDC_KI                          1031
#define IDC_KD                          1032
#define IDC_BUTTON6                     1033
#define IDC_BUTTON7                     1034
#define IDC_EDIT5                       1035
#define IDC_SPIN6                       1036
#define IDC_EDIT6                       1037
#define IDC_SPIN7                       1038
#define IDC_EDIT7                       1039
#define IDC_SPIN8                       1040
#define IDC_KP2                         1041
#define IDC_KI2                         1042
#define IDC_KD2                         1043
#define IDC_SLIDER3                     1046
#define IDC_STATIC_VOLTS                1047

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1048
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
