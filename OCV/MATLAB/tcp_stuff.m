clear all
clc
%Requests
DATA_PHASE                  = 1;
DATA_PHASE_RAW              = 2;
DATA_FACE_COORD_REF         = 3;
DATA_FACE_COORD_CURRENT     = 4;
DATA_FACE_CMD_RATIO_RAW     = 5;
DATA_FACE_CMD_FINAL         = 6;
DATA_MOTOR_COMMAND_REF      = 7;
DATA_MOTOR_MAX_VOLT         = 8;
DATA_MOTOR_OMEGA            = 9;
DATA_MOTOR_COMMAND_FINAL    = 10;
%----Connection----
PORT                        = 2712;
ADDR                        = '192.168.1.45';

t=tcpclient(ADDR,PORT);
cmd_hist=0;
face_ratio_hist = 0;
while(1)
    %Do the acquisition
    face_cmd = request(t, DATA_FACE_COORD_CURRENT, 2);
    face_ref = request(t, DATA_FACE_COORD_REF, 2);
    face_ratio = request(t, DATA_FACE_CMD_RATIO_RAW, 2);
    cmd_ref  = request(t, DATA_MOTOR_COMMAND_REF, 2);
    wheel_values = request(t,DATA_PHASE_RAW,1000*4);
    subplot(221);
    h = plot(640-face_cmd(1),640-face_cmd(2));
    axis([0 640 0 640])
    set(h,'Marker','square');
    set(h,'MarkerSize',20);
    hold on;
    h = plot(640-face_ref(1),640-face_ref(2));
    axis([0 640 0 640])
    set(h,'Marker','square');
    set(h,'MarkerSize',20);
    hold off;
    
       
    cmd_hist=[cmd_hist,cmd_ref];
    face_ratio_hist = [face_ratio_hist,face_ratio];
    subplot(212);
    plot(cmd_hist(1:2:length(cmd_hist)));
    hold on;
    plot(cmd_hist(2:2:length(cmd_hist)));
    hold off;
    
    subplot(222);
    plot(face_ratio_hist(1:2:length(face_ratio_hist)));
    hold on;
    plot(face_ratio_hist(2:2:length(face_ratio_hist)));
    hold off;
    
end