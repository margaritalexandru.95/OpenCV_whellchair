
function val = request(conn,request,len)
val=0;
STATUS_RECEIVED             = 254;
write(conn,uint8(request));
val=read(conn,len,'double');
write(conn,uint8(STATUS_RECEIVED));