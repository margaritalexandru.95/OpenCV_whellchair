#pragma once
typedef struct
{
    float K;
    float P;
    float x_est;
    float R;
    float Q;
}kalman_data;

/*prototypes*/
void kalman_init(kalman_data *kd);
float kalman_update(kalman_data *kd, float val_in);
